package main

import (
	"bufio"
	"encoding/xml"
	"fmt"
	"github.com/labstack/gommon/log"
	"io"
	"net"
	"os"
	"routes"
	"strconv"
	"strings"
	"time"
)

/* declaration of structure for xml-record */
type Path struct {
	Type       string   `xml:"type"`      /* type of xml-record */
	NodeBegin  int      `xml:"nodeBegin"` /* beginning node */
	NodeEnd    int      `xml:"nodeEnd"`   /* end node */
	Distance   int      `xml:"distance"`  /* distance of path */
	Nodes      string   `xml:"nodes"`     /* nodes of path */
}

const (
	numTown    = 15           /* number of town */
	errfile    = "error.log"  /* path to error logfile */
	graphfile  = "time.txt"   /* file of graph */
	tcp        = "tcp"        /* tcp-connection */
	port       = ":15000"     /* number of port */
	typeReq    = "request"    /* xml-record for request */
	typeAns    = "answer"     /* xml-record for answer */
	size       = 2028         /* size of query */
)

/* function to handle error */
func handleError(err error) bool {

	/* handle error */
	if err != nil {

		var (
			fileErr *os.File
		)

		/* write into error logfile */
		fileErr, _ = os.OpenFile(errfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		_, _ = fileErr.WriteString(time.Now().String() + ":" + err.Error() + "\n")

		/* error exists */
		return true

	}

	/* error does not exist */
	return false

}

/* function create two-dimensional array for graph */
func createGraph(amount int, graph *[][]int) {

	var (
		i int /* iterator */
	)

	/* creation of graph */
	*graph = make([][]int, amount)
	i = 0
	for i < amount {
		(*graph)[i] = make([]int, amount)
		i++
	}

}

/* function to read graph from file */
func readGraph(graph *[][]int) {

	var (
		fileGraph  *os.File      /* file which contains table of graph */
		err        error         /* variable for error */
		reader     *bufio.Reader /* reader of file */
		line       []byte        /* byte array of one line */
		row        string        /* string of one line of text */
		substrings []string      /* substrings of row (separator " ") */
		i, i1      int           /* iterators */
		currDist   int           /* current distance */
	)

	/* open file */
	fileGraph, err = os.Open(graphfile)
	handleError(err)

	/* create reader */
	reader = bufio.NewReader(fileGraph)
	i1 = 0
	/* read file */
	for {

		/* read one line */
		line, _, err = reader.ReadLine()
		row = string(line)

		/* handle error */
		if err == io.EOF {
			break
		} else {
			if handleError(err) {
				log.Fatal(err)
			}
		}

		/* create substrings */
		substrings = strings.Split(row, " ")
		i = 0
		/* save weight of graph */
		for i < numTown {
			currDist, err = strconv.Atoi(substrings[i])
			handleError(err)
			fmt.Printf("%s ", strconv.Itoa(currDist))
			(*graph)[i1][i] = currDist
			i++
		}
		fmt.Println()

		i1++

	}

}

/* handle query of client */
func handleQuery(conn net.Conn) {

	var (
		graph              [][]int      /* graph to find path */
		nodeBegin, nodeEnd int          /* node of beginning and end */
		dist               int          /* distance of path */
		past               []int        /* array of past nodes */
		inputArray         []byte       /* input byte array of query */
		outputArray        []byte       /* output byte array of answer */
		err                error        /* variable for error */
		path               Path         /* instance of xml-record */
		i                  int          /* iterator */
		nodes              string       /* nodes of path */
	)

	/* close connection */
	defer conn.Close()

	/* read byte array */
	inputArray = make([]byte, size)
	_, err = conn.Read(inputArray)
	if handleError(err) {
		return
	}

	/* receive instance of structure from byte array */
	err = xml.Unmarshal(inputArray, &path)
	if handleError(err) {
		return
	}

	/* check field of xml-record */

	/* type of request */
	if path.Type != typeReq  {
		if handleError(fmt.Errorf("type of xml-record should be request")) {
			return
		}
	}

	/* beginning node of request */
	if path.NodeBegin < 0 || path.NodeBegin > 14 {
		if handleError(fmt.Errorf("beginning node should be from 0 to 14")) {
			return
		}
	}

	/* end node of request */
	if path.NodeEnd < 0 || path.NodeEnd > 14 {
		if handleError(fmt.Errorf("end node should be from 0 to 14")) {
			return
		}
	}

	/* take begging and end nodes from xml-record */
	nodeBegin = path.NodeBegin
	nodeEnd = path.NodeEnd

	/* create array for graph */
	createGraph(numTown, &graph)
	/* read graph from file */
	readGraph(&graph)
	/* find the shortest path */
	dist, past = routes.FindShortestPath(nodeBegin, nodeEnd, &graph)

	/* write nodes for shortest route using array of past nodes */
	i = nodeEnd
	nodes = ""
	for past[i] != i {
		nodes += fmt.Sprint(strconv.Itoa(i) + " ")
		i = past[i]
	}

	/* change field of xml-record for answer */
	path.Type = typeAns
	path.Distance = dist
	path.Nodes = nodes

	/* give answer */
	outputArray, err = xml.MarshalIndent(path,"", " ")
	_, err = conn.Write(outputArray)
	if handleError(err) {
		return
	}


}

func main() {

	var (
		listener           net.Listener /* listener for socket */
		conn               net.Conn     /* connection of socket */
		err                error        /* variable for error */
	)

	/* creation of listener to listen port */
	listener, err = net.Listen(tcp, port)
	if handleError(err) {
		return
	}

	/* listen given port */
	for {

		/* creation of connection */
		conn, err = listener.Accept()
		_ = handleError(err)
		/* transfer connection processing to function */
		go handleQuery(conn)

	}

}
